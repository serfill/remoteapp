Function New-RemoteApp {
    param(
        [Parameter(Mandatory = $true, Position = 1, HelpMessage = 'Application Name')][string]$Name,
        [Parameter(Mandatory = $true, Position = 2, HelpMessage = 'Application Path')][string]$AppPath,
        [Parameter(Mandatory = $true, Position = 3, HelpMessage = 'Out Path')][string]$OutPath,
        [Parameter(Mandatory = $true, Position = 4, HelpMessage = 'Server Address')][string]$Server,
        [Parameter(Mandatory = $false, Position = 5, HelpMessage = 'Port')][int]$port = 3389
    )
    $AppPath = $AppPath.replace("\", "\\")
    $reg = 'Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Terminal Server\TSAppAllowList\Applications\' + $Name + ']
"CommandLineSetting"=dword:00000000
"RequiredCommandLine"=""
"IconIndex"=dword:00000000
"IconPath"="' + $AppPath + '"
"Path"="' + $AppPath + '"
"VPath"="' + $AppPath + '"
"ShowInTSWA"=dword:00000001
"Name"="' + $Name + '"
"SecurityDescriptor"=""'

    $rdp = 'redirectclipboard:i:1
redirectposdevices:i:0
redirectprinters:i:1
redirectcomports:i:1
redirectsmartcards:i:1
devicestoredirect:s:*
drivestoredirect:s:*
redirectdrives:i:1
session bpp:i:32
prompt for credentials on client:i:1
span monitors:i:1
use multimon:i:1
remoteapplicationmode:i:1
server port:i:' + $port + ' 
allow font smoothing:i:1
promptcredentialonce:i:1
authentication level:i:2
gatewayusagemethod:i:2
gatewayprofileusagemethod:i:0
gatewaycredentialssource:i:0
full address:s:' + $Server + ' 
alternate shell:s:||' + $AppPath + '
remoteapplicationprogram:s:||' + $Name + '
gatewayhostname:s:
remoteapplicationname:s:' + $name + '
remoteapplicationcmdline:s:'

    $reg | Out-File -FilePath ($OutPath + $Name + '.reg') -Force
    $rdp | Out-File -FilePath ($OutPath + $Name + '.rdp') -Force
}

New-RemoteApp -Name "Jopa" -AppPath "C:\Hyper-V\DOCKER\Virtual Hard Disks\DOCKER.vhdx" -OutPath "C:\Code\RemoteApp\"
